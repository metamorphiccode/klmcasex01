package com.klmcase.x01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Klmcasex01Application {

	public static void main(String[] args) {
		SpringApplication.run(Klmcasex01Application.class, args);
	}
}
