package com.klmcase.x01.services;

import java.util.Collection;
import java.util.Locale;

import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.klmcase.x01.domains.Location;

@Service
public class OAuthSecurityApplicationService extends OAuthAbstractSecurityApplicationService{


	public PagedResources<Resource<Location>> list(Locale locale, int page , int size) {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer " + OAuthAbstractSecurityApplicationService.getAccessToken().getValue());
			HttpEntity<String> entity  = new HttpEntity<>(headers);
			ResponseEntity<PagedResources<Resource<Location>>> list = null;
			PagedResources<Resource<Location>> objs = restTemplate.exchange("http://192.168.196.129:8080/airports",HttpMethod.GET,entity,PagedResources<Resource<Location>>.class).getBody();
			return list.getBody();
	}




	public static OAuth2AccessToken getAccessToken() {
		return OAuthAbstractSecurityApplicationService.getAccessToken();
	}


}
