package com.klmcase.x01.services;

import java.util.Arrays;

import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

public abstract class OAuthAbstractSecurityApplicationService {



	protected static OAuth2AccessToken getAccessToken() {

		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
		resource.setAccessTokenUri("http://192.168.196.129:8080/oauth/token");
		resource.setClientId("travel-api-client");
		resource.setId("locations-api");
		resource.setClientSecret("psw");
		resource.setScope(Arrays.asList("read", "write", "trust"));

		ClientCredentialsAccessTokenProvider provider = new ClientCredentialsAccessTokenProvider();
		OAuth2AccessToken accessToken = provider.obtainAccessToken(resource, new DefaultAccessTokenRequest());
		return accessToken;

	}

}
