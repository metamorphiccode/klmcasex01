package com.klmcase.x01.domains;


import lombok.Value;

@Value
public class Coordinates {

    private double latitude, longitude;

}
