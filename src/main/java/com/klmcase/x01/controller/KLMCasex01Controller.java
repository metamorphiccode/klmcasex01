package com.klmcase.x01.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Collection;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.exercises.spring.locations.Location;
import com.klmcase.x01.services.OAuthSecurityApplicationService;

@RestController
@RequestMapping("/klmcasexo1")
public class KLMCasex01Controller {

	@Autowired
	private OAuthSecurityApplicationService oAuthSecurityApplicationService;

	@RequestMapping(method = GET,value="/accessToken")
	public OAuth2AccessToken getAccessToken() {
		return OAuthSecurityApplicationService.getAccessToken();
	}

	@RequestMapping(method = GET)
	public HttpEntity<PagedResources<Resource<Location>>> airports(@RequestParam(value = "lang", defaultValue = "en") String lang, int page, int size) {
		Collection collection = oAuthSecurityApplicationService.list(Locale.forLanguageTag(lang), page, size);
		System.out.println(collection.toString());
		return "hello world";

	}
}
